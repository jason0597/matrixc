```
                  _        _          
                 | |      (_)         
  _ __ ___   __ _| |_ _ __ ___  _____ 
 | '_ ` _ \ / _` | __| '__| \ \/ / __|
 | | | | | | (_| | |_| |  | |>  < (__ 
 |_| |_| |_|\__,_|\__|_|  |_/_/\_\___|
                                      
                                      
```

Testing ground for the related libraries for building the Matrix C++ homeserver.

Here we'll decide on the final libraries to be used and the overall structure of the project.

# Goals
Top goal is that this server is lightweight! It would be bad if we had a massive SQL dependency like PostgreSQL or MySQL or RocksDB. SQLite shall do. Apply similar logic to other design considerations.

This server must be easy to deploy. That means we will have to use libraries that are easily available in distros, though it is up for debate if a library is too good to pass up.

# Libraries used

* [SQLite](https://www.sqlite.org/index.html) for storing all the data a Matrix homeserver needs
* [Boost::Beast](https://www.boost.org/doc/libs/1_73_0/libs/beast/doc/html/index.html) for HTTP handling 
* [json-c](https://github.com/json-c/json-c) for JSON parsing
* [OpenSSL](https://www.openssl.org/) for TLS handling of HTTPS connections
* [Olm](https://gitlab.matrix.org/matrix-org/olm/) for handling of E2E cryptography between devices
